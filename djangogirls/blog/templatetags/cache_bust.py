import time
from django import template

register = template.Library()

@register.simple_tag()
def cache_bust():
  return int(time.time())
